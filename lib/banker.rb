require "banker/version"
require 'rest_client'

module Banker
  # Your code goes here...
  BASE_URL = 'http://bank.arjanfrans.nl'

  ACCOUNT_RESOURCE_URL = BASE_URL + '/accounts'
  TRANSACTION_RESOURCE_URL = BASE_URL + '/transactions'

  class Account

    def self.create(email, paypal_email, iban)
      RestClient.get(BASE_URL + '/accounts_exists', {:params => { :email => email }}) { |response, request, result, &block|
        puts response
        case response.code
          when 404
            RestClient.post(ACCOUNT_RESOURCE_URL,
                            {
                                account: {
                                    email: email,
                                    paypal: paypal_email,
                                    iban: iban.to_s
                                }
                            }.to_json, :content_type => :json, :accept => :json) { |response2, request2, result2, &block2|
              puts response2
              case response2.code
                when 201
                  response2
                else
                  response.return!(request2, result2, &block2)
              end
            }
          else
            response
        end
      }
    end


  end

  class Transaction


    def self.find_by_id(id)
      RestClient.get(TRANSACTION_RESOURCE_URL + '/' + id.to_s) { |response, request, result, &block|
        case response.code
          when 200
            response
          else
            response.return!(request, result, &block)
        end
      }
    end

    def self.find_all_by_email(email)
      RestClient.get(TRANSACTION_RESOURCE_URL, {:params => {:email => email}}) { |response, request, result, &block|
        puts request.url
        case response.code
          when 200
            response
          else
            response.return!(request, result, &block)
        end
      }
    end

    def self.create(from_email, to_email, amount)
      RestClient.post(TRANSACTION_RESOURCE_URL,
                      {
                          transaction: {
                              from_attributes: {
                                  email: from_email
                              },
                              to_attributes: {
                                  email: to_email
                              },
                              amount: amount
                          }
                      }.to_json, :content_type => :json, :accept => :json) { |response, request, result, &block|
        puts response
        case response.code
          when 201
            response
          else
            response.return!(request, result, &block)
        end
      }
    end


  end
end
