require 'rspec'
require 'banker'

describe 'create Transaction' do

  it 'should return 201' do
    response = Banker::Transaction.create('arjan@arjanfrans.nl', 'frans@arjanfrans.nl', 20)
    puts response
  end

  it 'should return 200' do
    response = Banker::Transaction.find_by_id(1)
    puts response
  end

  it 'should return results' do
    response = Banker::Transaction.find_all_by_email("arjan@arjanfrans.nl")
    puts response
  end
end

describe 'create account' do

  it 'should return 201' do
    response = Banker::Account.create('affffff@arjanfrans.nl', 'ffffs@arjanfrans.nl', 'YWJSN495BRXGHEZPDM')
    puts response
  end
end